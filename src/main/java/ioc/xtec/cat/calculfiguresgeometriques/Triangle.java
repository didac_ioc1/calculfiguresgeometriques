/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

/**
 *
 * @author didac-padilla-marsi-ach
 */

import java.util.Scanner;    

public class Triangle implements FiguraGeometrica{
    private final double costat;
    private final double alcada;
    
    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud dels costats del triangle: ");
        this.costat = scanner.nextDouble();
        System.out.println("Introduïu la longitud de l'alçada del triangle: ");
        this.alcada = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return (costat * alcada) / 2;
    }
    
    @Override
    public double calcularPerimetre() {
        return 3 * costat;
    }
}

